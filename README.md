# Docker Packer kvm

According to the work of [Goffinet](https://github.com/goffinet/packer-kvm) (MIT License).

This project allows to :

* build a docker image embedding Packer and qemu (the builder)
* build VM images in qcow2 format (with the Docker builder)

## How to build

To build the Docker image locally :

```
cd docker && docker build -t packer-kvm .
```

To start building the Debian 12 kvm image for example :

```
cd ../
docker run --rm \
  --name debian12-builder \
  -e PACKER_LOG=1 \
  -e PACKER_LOG_PATH="packer-docker.log" \
  -d --privileged --cap-add=ALL \
  -v /lib/modules:/lib/modules \
  -v `pwd`:/opt/ \
  -w /opt/ packer-kvm \
  build -var-file="debian12.0.pkrvars.hcl" debian.pkr.hcl
```

## License

* GNU GPL v3

## Links

* [Debian Docker install](https://docs.docker.com/engine/install/debian/)
* [Docker post install](https://docs.docker.com/engine/install/linux-postinstall/)
* [Run the Docker daemon as a non-root user (Rootless mode)](https://docs.docker.com/engine/security/rootless/)
* [QEMU Builder](https://developer.hashicorp.com/packer/plugins/builders/qemu)
* [Goffinet packer KVM](https://github.com/goffinet/packer-kvm)
* [Packer docker hub images](https://github.com/hashicorp/docker-hub-images)