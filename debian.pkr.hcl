variable "iso_checksum" {
  type    = string
  default = ""
}

variable "iso_url" {
  type    = string
  default = ""
}

variable "name" {
  type    = string
  default = ""
}

variable "version" {
  type    = string
  default = ""
}

variable "config_file" {
  type    = string
  default = "debian-preseed.cfg"
}

variable "cpu" {
  type    = string
  default = "2"
}

variable "disk_size" {
  type    = string
  default = "20000"
}

variable "headless" {
  type    = string
  default = "true"
}

variable "ram" {
  type    = string
  default = "2048"
}

variable "ssh_password" {
  type    = string
  default = "changeme"
}

variable "ssh_username" {
  type    = string
  default = "root"
}

source "qemu" "debian" {
  accelerator      = "kvm"
  boot_command     = [
    "<esc><wait>", "auto <wait>", "console-keymaps-at/keymap=fr <wait>",
    "console-setup/ask_detect=false <wait>", "debconf/frontend=noninteractive <wait>",
    "debian-installer=fr_FR <wait>", "fb=false <wait>", "install <wait>",
    "kbd-chooser/method=fr <wait>", "keyboard-configuration/xkb-keymap=fr <wait>",
    "locale=fr_FR <wait>", "netcfg/get_hostname=${var.name}${var.version}-tpl <wait>",
    "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/http/${var.config_file} <wait>",
    "<enter><wait>"]
  boot_wait        = "15s"
  disk_cache       = "none"
  disk_compression = true
  disk_discard     = "unmap"
  disk_interface   = "virtio-scsi"
  disk_size        = var.disk_size
  format           = "qcow2"
  headless         = var.headless
  host_port_max    = 2229
  host_port_min    = 2222
  http_directory   = "."
  http_port_max    = 10089
  http_port_min    = 10082
  iso_checksum     = var.iso_checksum
  iso_url          = var.iso_url
  net_device       = "virtio-net"
  output_directory = "artifacts/${var.name}${var.version}"
  vm_name          = "${var.name}${var.version}-tpl.qcow2"
  qemu_binary      = "/usr/bin/qemu-system-x86_64"
  qemuargs         = [["-m", "${var.ram}M"], ["-smp", "${var.cpu}"]]
  shutdown_command = "echo '${var.ssh_password}' | sudo -S shutdown -P now"
  ssh_password     = var.ssh_password
  ssh_username     = var.ssh_username
  ssh_timeout = "30m"
}

build {
  sources = ["source.qemu.debian"]

  provisioner "shell" {
    execute_command = "{{ .Vars }} bash '{{ .Path }}'"
    inline          = ["apt-get update", "apt-get -y install ansible"]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/playbook.yml"
  }

  provisioner "shell" {
    execute_command = "{{ .Vars }} bash '{{ .Path }}'"
    inline          = ["apt-get -y purge ansible", "apt-get clean", "apt-get -y autoremove --purge"]
  }
}
